<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\usuario;
use Session;


class UsuarioSenha extends Controller
{
public function cadastro(){
        
    return view('cadastraDados');
}

public function dados(Request $request){
   $usuario = $request->input('usuario');
   $email = $request->input('email');
   $senha = $request->input('senha');

return view('apresentaDados', ['usuario'=>$usuario, 'email'=>$email, 'senha'=>$senha]);
}
}
