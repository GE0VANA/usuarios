<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioSenha;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [UsuarioSenha::class, 'cadastro']);
Route::get('/cadastraDados', [UsuarioSenha::class, 'cadastro']);
Route::get('/apresentaDados', [UsuarioSenha::class, 'dados']);
Route::post('/', [UsuarioSenha::class, 'cadastro']);
Route::post('/apresentaDados', [UsuarioSenha::class, 'dados']);
Route::post('/cadastraDados', [UsuarioSenha::class, 'dados']);

